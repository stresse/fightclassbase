﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using CombatRotation.RotationFramework;
using robotManager;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager; 
using wManager.Wow.Bot.States;
using wManager.Events;
using wManager;
using wManager.Wow.Enums;
using Uhtred;
//using Uhtred.Specials;

public class Main : ICustomClass
{
    public float Range => NeedsRangePull() ? 40f : 5f;


    private bool _isLaunched;
    private static WoWLocalPlayer Me = ObjectManager.Me;

    internal static Logger Logger { get; set; }


    private static RotationSpell Attack             = new RotationSpell("Attack");
    private static RotationSpell DesperatePrayer    = new RotationSpell("Desperate Prayer");
    private static RotationSpell DivineStar         = new RotationSpell("Divine Star");
    private static RotationSpell Evangelism         = new RotationSpell("Evangelism");
    private static RotationSpell Halo               = new RotationSpell("Halo");
    private static RotationSpell HolyNova           = new RotationSpell("Holy Nova");
    private static RotationSpell Levitate           = new RotationSpell("Levitate");
    private static RotationSpell PainSuppression    = new RotationSpell("Pain Suppression");
    private static RotationSpell Penance            = new RotationSpell("Penance");
    private static RotationSpell PowerWordBarrier   = new RotationSpell("Power Word: Barrier");
    private static RotationSpell PowerWordFortitude = new RotationSpell("Power Word: Fortitude");
    private static RotationSpell PowerWordRadiance  = new RotationSpell("Power Word: Radiance");
    private static RotationSpell PowerWordSolace    = new RotationSpell("Power Word: Solace");
    private static RotationSpell PowerWordShield    = new RotationSpell("Power Word: Shield");
    private static RotationSpell PurgetheWicked     = new RotationSpell("Purge the Wicked");
    private static RotationSpell Purify             = new RotationSpell("Purify");
    private static RotationSpell Rapture            = new RotationSpell("Rapture");
    private static RotationSpell Schism             = new RotationSpell("Schism");
    private static RotationSpell Shadowfiend        = new RotationSpell("Shadowfiend");
    private static RotationSpell ShadowMend         = new RotationSpell("Shadow Mend");
    private static RotationSpell ShadowWordPain     = new RotationSpell("Shadow Word: Pain");
    private static RotationSpell Smite              = new RotationSpell("Smite");

    private List<RotationStep> RotationSpells = new List<RotationStep>{
        new RotationStep(Attack,         0.5f, (s, t) => RotationFramework.IsCast && !RotationCombatUtil.IsAutoAttacking(), RotationCombatUtil.BotTarget),
        new RotationStep(Penance,        0.9f, (s, t) => Me.InCombat,                                                       RotationCombatUtil.BotTarget),
        new RotationStep(ShadowWordPain, 10f,  (s, t) => !t.HasBuff(ShadowWordPain.Spell.NameInGame),                       RotationCombatUtil.BotTarget),
    };

//        new RotationStep(new RotationSpell("Lightning Bolt", 1), 0.95f, (s, t) => !Me.InCombatFlagOnly && NeedsRangePull() && t.HealthPercent == 100, RotationCombatUtil.BotTarget),
/*        
        new RotationStep(new RotationSpell("Healing Wave"), 1.0f, (s, t) => t.HealthPercent < 35, RotationCombatUtil.FindMe),
        new RotationStep(new RotationSpell("Earth Shock", 1), 1.1f, (s, t) => t.IsCasting() && !CanShock(), RotationCombatUtil.BotTarget),
        new RotationStep(new RotationSpell("Grounding Totem"), 1.1f, (s, t) => true, RotationCombatUtil.FindEnemyCastingOnMe),
        new RotationStep(new RotationSpell("Tremor Totem"), 1.1f, (s, t) => t.CastingSpell("Fear", "Terrify", "Howl of Terror", "Hibernate", "Scare Beast"), RotationCombatUtil.FindEnemyCasting, true),
        new RotationStep(new RotationSpell("Stoneclaw Totem"), 1.1f, (s, t) => RotationFramework.Units.Where(u => u.Reaction == Reaction.Hostile && u.IsAlive && u.GetDistance < 8).ToList().Count >= 2 && !HasTotem("Stoneclaw Totem"), RotationCombatUtil.FindMe),
        new RotationStep(new RotationSpell("Shamanistic Rage"), 2.1f, (s, t) => Me.ManaPercentage < 50 && t.HealthPercent > 80 && t.GetDistance <= 8, RotationCombatUtil.FindMe),
        new RotationStep(new RotationSpell("Shamanistic Rage"), 2.1f, (s, t) => RotationFramework.Units.Count(o => o.IsAlive && o.Reaction == Reaction.Hostile && o.GetDistance <= 8) >= 2, RotationCombatUtil.FindMe),
        new RotationStep(new RotationSpell("Stormstrike"), 10f, (s, t) => true, RotationCombatUtil.BotTarget),
        new RotationStep(new RotationSpell("Flame Shock"), 10.1f, (s, t) => CanShock() && !t.HasBuff("Flame Shock") && t.HealthPercent > 40, RotationCombatUtil.BotTarget),
        new RotationStep(new RotationSpell("Earth Shock"), 10.2f, (s, t) => (CanShock() || t.IsCasting()) && t.HasBuff("Stormstrike"), RotationCombatUtil.BotTarget),
        new RotationStep(new RotationSpell("Frost Shock"), 11f, (s, t) => CanShock(), RotationCombatUtil.BotTarget),
        new RotationStep(new RotationSpell("Earth Shock"), 12, (s, t) => CanShock(), RotationCombatUtil.BotTarget),
        new RotationStep(CurePoison, 16f, (s, t) => t.HasDebuffType("Poison") && t.ManaPercentage >= 30, RotationCombatUtil.FindMe),
       
    };
    */

    public void Initialize()
    {
        PriestSettings.Load();
        wManagerSetting.CurrentSetting.UseLuaToMove = true;
        Logger = new Logger("Priest");        
        RotationFramework.Initialize(PriestSettings.CurrentSetting.SlowRotation, PriestSettings.CurrentSetting.FrameLock);
        _isLaunched = true;

        RotationSpells.Sort((a, b) => a.Priority.CompareTo(b.Priority));
        Rotation();
    }


    public void Rotation()
    {
        while (_isLaunched)
        {
            try
            {
                if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause && !Me.IsDead)
                {

                    UseBuffs();
                    if(Fight.InFight)
                    {
                        RotationFramework.RunRotation(RotationSpells);
                    }
                }
            }
            catch(Exception e)
            {
                Logging.WriteError("ExampleClass ERROR:" + e);
            }
            
            Thread.Sleep(Usefuls.LatencyReal);
        }
    }



    private void UseBuffs()
    {
        // we don't want the bot to dismount while we are having it gathering herbs or moving continents 
        // during questing -- 
        if (Me.IsMounted || Me.InCombatFlagOnly || Fight.InFight || Me.HasBuff("Resurrection Sickness"))
            return;

        if (Me.HasDebuffType("Magic"))
        {
            RotationCombatUtil.CastSpell(Purify, Me);
        }
        
        if (Me.HasDebuffType("Disease"))
        {
            RotationCombatUtil.CastSpell(Purify, Me);
        }

        if (Me.HasBuff(PowerWordFortitude.Spell.NameInGame))
        {
            RotationCombatUtil.CastBuff(PowerWordFortitude);
        }

        if (Me.HealthPercent < 60)
        {
            RotationCombatUtil.CastSpell(ShadowMend, Me);
        }
        
    }

    
    private static bool NeedsRangePull()
    {
        if (Fight.CombatStartSince > 8000)
        {
            return false;
        }
        
        return RotationFramework.Units.FirstOrDefault(o =>
                   o.IsAlive && o.Reaction == Reaction.Hostile &&
                   o.Guid != RotationFramework.Target.Guid &&
                   o.Position.DistanceTo(RotationFramework.Target.Position) <= 38) != null;
    }

    public void Dispose()
    {
        _isLaunched = false;
        RotationFramework.Dispose();
        
//        MovementEvents.OnMovementPulse -= LongGhostWolfHandler;
//        MovementEvents.OnMoveToPulse -= GhostWolfHandler;
    }

    public void ShowConfiguration()
    {
        PriestSettings.Load();
        PriestSettings.CurrentSetting.ToForm();
        PriestSettings.CurrentSetting.Save();
    }

    private void Log(params object[] inputs)
    {
        Logger.Log(inputs);
    }
}

/*
 * SETTINGS
*/

[Serializable]
public class PriestSettings : Settings
{
    
    [Setting]
    [DefaultValue(true)]
    [Category("General")]
    [DisplayName("Framelock")]
    [Description("Lock frames before each combat rotation (can help if it skips spells)")]
    public bool FrameLock { get; set; }
    
    [Setting]
    [DefaultValue(false)]
    [Category("General")]
    [DisplayName("Slow rotation for performance issues")]
    [Description("If you have performance issues with wRobot and the fightclass, activate this. It will try to sleep until the next spell can be executed. This can and will cause some spells to skip.")]
    public bool SlowRotation { get; set; }


    public PriestSettings()
    {
        FrameLock = true;
        SlowRotation = false;
    }

    public static PriestSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("ExampleSettings > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<PriestSettings>(AdviserFilePathAndName("CustomClass-ExampleSettings",
                                                                 ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new PriestSettings();
        }
        catch (Exception e)
        {
            Logging.WriteError("ExampleSettings > Load(): " + e);
        }
        return false;
    }
}