﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using CombatRotation.RotationFramework;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager.Wow.Bot.States;
using wManager.Events;
using wManager;
using wManager.Wow.Enums;

public class Main : ICustomClass
{
    public float Range => ObjectManager.Me.IsMounted? 2f : 40f;


    private bool _isLaunched;
    private static WoWLocalPlayer Me = ObjectManager.Me;


    // Rotation Spells
    private static RotationSpell Attack             = new RotationSpell("Attack");
    private static RotationSpell DesperatePrayer    = new RotationSpell("Desperate Prayer");
    private static RotationSpell DispelMagic         = new RotationSpell("Dispel Magic");
    private static RotationSpell DivineStar = new RotationSpell("Divine Star");
    private static RotationSpell Evangelism         = new RotationSpell("Evangelism");
    private static RotationSpell Halo               = new RotationSpell("Halo");
    private static RotationSpell HolyNova           = new RotationSpell("Holy Nova");
    private static RotationSpell Levitate           = new RotationSpell("Levitate");
    private static RotationSpell PainSuppression    = new RotationSpell("Pain Suppression");
    private static RotationSpell Penance            = new RotationSpell("Penance");
    private static RotationSpell PowerWordBarrier   = new RotationSpell("Power Word: Barrier");
    private static RotationSpell PowerWordFortitude = new RotationSpell("Power Word: Fortitude");
    private static RotationSpell PowerWordRadiance  = new RotationSpell("Power Word: Radiance");
    private static RotationSpell PowerWordShield    = new RotationSpell("Power Word: Shield");
    private static RotationSpell PowerWordSolace    = new RotationSpell("Power Word: Solace");
    private static RotationSpell PurgetheWicked     = new RotationSpell("Purge the Wicked");
    private static RotationSpell Purify             = new RotationSpell("Purify");
    private static RotationSpell Rapture            = new RotationSpell("Rapture");
    private static RotationSpell Schism             = new RotationSpell("Schism");
    private static RotationSpell Shadowfiend        = new RotationSpell("Shadowfiend");
    private static RotationSpell ShadowMend         = new RotationSpell("Shadow Mend");
    private static RotationSpell ShadowWordPain     = new RotationSpell("Shadow Word: Pain");
    private static RotationSpell Smite              = new RotationSpell("Smite");

    private List<RotationStep> RotationSpells = new List<RotationStep>
    {

        new RotationStep
        (
            spell: Attack,
            priority: 0.5f,
            predicate: (s, t) => RotationFramework.IsCast && !RotationCombatUtil.IsAutoAttacking(),
            targetFinder: RotationCombatUtil.BotTarget
        ),

        new RotationStep
        (
            spell: ShadowWordPain,
            priority: 0.5f,
            predicate: (s, t) => RotationFramework.IsCast && !RotationCombatUtil.IsAutoAttacking(),
            targetFinder: RotationCombatUtil.BotTarget
        ),

/*
        new RotationStep(spell: new RotationSpell("Gift of the Naaru"), priority: 0.9f, predicate: (s, t) => t.HealthPercent < 70 && t.ManaPercentage < 35 && RotationFramework.Target.HealthPercent > 80, targetFinder: RotationCombatUtil.FindMe),
        new RotationStep(spell: new RotationSpell("Blood Fury"), 0.9f, (s, t) => t.ManaPercentage < 10, RotationCombatUtil.FindMe),
        new RotationStep(spell: new RotationSpell("Lightning Bolt", 1), priority : 0.95f, predicate :(s, t) => ! Me.InCombatFlagOnly && NeedsRangePull() && t.HealthPercent == 100, targetFinder : RotationCombatUtil.BotTarget),
        new RotationStep(spell: new RotationSpell("Healing Wave"), 1.0f, (s, t) => t.HealthPercent < 35, RotationCombatUtil.FindMe),
        new RotationStep(spell: new RotationSpell("Earth Shock", 1), priority : 1.1f, predicate :(s, t) => t.IsCasting() && ! CanShock(), targetFinder : RotationCombatUtil.BotTarget),
        new RotationStep(spell: new RotationSpell("Grounding Totem"), priority: 1.1f, (s, t) => true, RotationCombatUtil.FindEnemyCastingOnMe),
*/
        new RotationStep(spell: Purify, priority: 16f, predicate: (s, t) => t.HasDebuffType("Disease"), targetFinder: RotationCombatUtil.FindMe),
        new RotationStep(spell: Purify, priority: 16f, predicate: (s, t) => t.HasDebuffType("Magic"), targetFinder: RotationCombatUtil.FindMe),
    };

    public void Initialize()
    {
        PriestSettings.Load();
        wManagerSetting.CurrentSetting.UseLuaToMove = true;
        
        RotationFramework.Initialize(PriestSettings.CurrentSetting.SlowRotation, PriestSettings.CurrentSetting.FrameLock);
        //MovementEvents.OnMovementPulse += LongGhostWolfHandler;
        //MovementEvents.OnMoveToPulse += GhostWolfHandler;
        _isLaunched = true;

        RotationSpells.Sort((a, b) => a.Priority.CompareTo(b.Priority));
        Rotation();
    }


    public void Rotation()
    {
        while (_isLaunched)
        {
            try
            {
                if (Conditions.InGameAndConnectedAndAliveAndProductStartedNotInPause && !Me.IsDead)
                {

                    UseBuffs();
                    
                    if(Fight.InFight)
                    {
                        /*
                        if(Me.HasBuff("Ghost Wolf") && RotationFramework.Units.Any(u => u.IsTargetingMe && !u.IsPlayer()))
                            Lua.RunMacroText("/cancelaura Ghost Wolf");
                            */

                        RotationFramework.RunRotation(RotationSpells);
                    }
                }
            }
            catch(Exception e)
            {
                Logging.WriteError("ExampleClass ERROR:" + e);
            }
            // Don't bother using resources thinking about actions before they can possibly happen ?
            Thread.Sleep(Usefuls.LatencyReal);
        }
    }
    private void UseBuffs()
    {

    }

    public void Dispose()
    {
        _isLaunched = false;
        RotationFramework.Dispose();
        
        //MovementEvents.OnMovementPulse -= LongGhostWolfHandler;
        //MovementEvents.OnMoveToPulse -= GhostWolfHandler;
    }

    public void ShowConfiguration()
    {
        PriestSettings.Load();
        PriestSettings.CurrentSetting.ToForm();
        PriestSettings.CurrentSetting.Save();
    }

}

/*
 * SETTINGS
*/

[Serializable]
public class PriestSettings : Settings
{
    
    [Setting]
    [DefaultValue(true)]
    [Category("General")]
    [DisplayName("Framelock")]
    [Description("Lock frames before each combat rotation (can help if it skips spells)")]
    public bool FrameLock { get; set; }
    
    [Setting]
    [DefaultValue(false)]
    [Category("General")]
    [DisplayName("Slow rotation for performance issues")]
    [Description("If you have performance issues with wRobot and the fightclass, activate this. It will try to sleep until the next spell can be executed. This can and will cause some spells to skip.")]
    public bool SlowRotation { get; set; }


    public PriestSettings()
    {
        FrameLock = true;
        SlowRotation = false;
    }

    public static PriestSettings CurrentSetting { get; set; }

    public bool Save()
    {
        try
        {
            return Save(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName));
        }
        catch (Exception e)
        {
            Logging.WriteError("ExampleSettings > Save(): " + e);
            return false;
        }
    }

    public static bool Load()
    {
        try
        {
            if (File.Exists(AdviserFilePathAndName("CustomClass-ExampleSettings", ObjectManager.Me.Name + "." + Usefuls.RealmName)))
            {
                CurrentSetting =
                    Load<PriestSettings>(AdviserFilePathAndName("CustomClass-ExampleSettings",
                                                                 ObjectManager.Me.Name + "." + Usefuls.RealmName));
                return true;
            }
            CurrentSetting = new PriestSettings();
        }
        catch (Exception e)
        {
            Logging.WriteError("ExampleSettings > Load(): " + e);
        }
        return false;
    }
}