﻿using System;
using System.Threading;
using System.Windows.Forms;
using robotManager.Helpful;
using robotManager.Products;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using MemoryRobot;
using Timer = robotManager.Helpful.Timer;using System.Collections.Generic;using Uhtred;public class Main : ICustomClass
{


    public float Range { get { return 40f; } }
    //    public bool 

    private bool _isLaunched;
    private Int128 _lastTarget;    internal static Logger Logger { get; set; }

    // UNITS
    public static WoWLocalPlayer Me => ObjectManager.Me;    // GUIDs    public static Int128 MouseoverGuid { get => Me.MouseOverGuid; set => Me.MouseOverGuid = value; }    // SPELLS    public static Spell Attack = new Spell("Attack");    public static Spell DesperatePrayer = new Spell("Desperate Prayer");    public static Spell DivineStar = new Spell("Divine Star");    public static Spell Evangelism = new Spell("Evangelism");    public static Spell Halo = new Spell("Halo");    public static Spell HolyNova = new Spell("Holy Nova");    public static Spell Levitate = new Spell("Levitate");    public static Spell PainSuppression = new Spell("Pain Suppression");    public static Spell Penance = new Spell("Penance");    public static Spell PowerWordBarrier = new Spell("Power Word: Barrier");    public static Spell PowerWordFortitude = new Spell("Power Word: Fortitude");    public static Spell PowerWordRadiance = new Spell("Power Word: Radiance");    public static Spell PowerWordSolace = new Spell("Power Word: Solace");    public static Spell PowerWordShield = new Spell("Power Word: Shield");    public static Spell PurgeTheWicked = new Spell("Purge the Wicked");    public static Spell Purify = new Spell("Purify");    public static Spell Rapture = new Spell("Rapture");    public static Spell Schism = new Spell("Schism");    public static Spell Shadowfiend = new Spell("Shadowfiend");    public static Spell ShadowMend = new Spell("Shadow Mend");    public static Spell ShadowWordPain = new Spell("Shadow Word: Pain");    public static Spell Smite = new Spell("Smite");    List<Spell> Buffs = new List<Spell>() { PowerWordFortitude, };    public void Initialize() // When product started, initialize and launch Fightclass
    {
        _isLaunched = true;        Logger = new Logger("Priest");        Logging.Write("[My fightclass] Is initialized.");
        Rotation();
    }    public void Dispose() // When product stopped
    {
        _isLaunched = false;
        Logging.Write("[My fightclass] Stop in progress.");
    }    public void ShowConfiguration() // When use click on Fight class settings
    {
        MessageBox.Show("[My fightclass] No setting for this Fight Class.");
    }    internal void Rotation()
    {
        Logging.Write("[My fightclass] Is started.");
        while (_isLaunched)
        {
            try
            {
                if (!Products.InPause)
                {
                    if (!ObjectManager.Me.IsDeadMe)
                    {
                        BuffRotation();                        if (Fight.InFight && ObjectManager.Target != null)
                        {
                            Pull();
                            CombatRotation();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logging.WriteError("[My fightclass] ERROR: " + e);
            }            Thread.Sleep(10); // Pause 10 ms to reduce the CPU usage.
        }
        Logging.Write("[My fightclass] Is now stopped.");
    }    internal void BuffRotation()
    {
        if (ObjectManager.Me.IsMounted)
            return;        foreach (Spell buff in Buffs)        {
            // Note: we are using 'WoWObject' instead of 'WoWUnit' because 'WoWUnit' is a subtype of 'WoWObject'
            // Google: "C# inheritance"




            if (buff.KnownSpell && buff.IsSpellUsable && !ObjectManager.Me.HaveBuff(buff.NameInGame))            {                buff.Launch(stopMove: false, waitIsCast: false, ignoreIfCast: false, castOnSelf: true);            }        }
    }
    internal void Pull()
    {                ShadowWordPain.Launch(false, false, false, "mouseover", true);
    }    internal void CombatRotation()
    {

    }

    private void Log(params object[] inputs)    {        Logger.Log(inputs);    }
}

public static class CombatUtility{    public static Func<WoWUnit, Spell, bool> needsBuff = (u, s) => u.Type == WoWObjectType.Player && u.HaveBuff(s.NameInGame);    public static Action<WoWObject> setMouseover = o => ObjectManager.Me.MouseOverGuid = o.Guid;    public static Action<WoWObject> setFocus = o => ObjectManager.Me.FocusGuid = o.Guid;    public static Action<Spell, WoWUnit> cast = (s, u) =>    {        setMouseover(u);        s.Launch(stopMove: false, waitIsCast: false, ignoreIfCast: false, luaUnitId: "mouseover");    };    public static Action<WoWUnit, Spell> castBuffIfNeeded = (u, b) =>    {        if (needsBuff(u, b))        {            cast(b, u);        }    };}