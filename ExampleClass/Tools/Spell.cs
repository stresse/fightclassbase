﻿using System;
using System.Collections.Generic;
using System.Linq;
using robotManager.Helpful;
using System.Text;
using wManager.Wow;
using wManager.Wow.ObjectManager;
using wManager.Wow.Helpers;
using wManager.Wow.Class;
using MemoryRobot;
using Uhtred;
using wManager.Wow.Enums;

namespace Uhtred
{
    public static partial class Extensions
    {
        public static readonly List<string> AoESpells = new List<string>(){
        "Halo",
        "Holy Nova",
        "Power Word: Barrier"
    };
        public static bool CastSpell(this Spell spell, WoWObject unit, bool force=false)
        {
            //	Handle bullshit units
            if (unit == null || !unit.IsValid || !spell.KnownSpell || !spell.IsSpellUsable)
            {
                Logging.Write("Bad spellcast, skipping");
                return false;
            }

            //	Check If Toon is Wanding
            if (spell.Name == "Shoot" && SpellManager.IsAutoRepeating(spell.Name))
            {
                return true;
            }

            //	Fuck off if toon is already casting and (force == false)
            if (ObjectManager.Me.IsCast && !force)
                return false;

            //	Stop moving if spell needs to be cast
            if (spell.CastTime > 0)
                MovementManager.StopMoveTo(false, Usefuls.LatencyReal);

            //	Force cast spell if (force == true)
            if (force)
                Lua.LuaDoString("SpellStopCasting();");

            //	Cast AoE spells by location
            if (AoESpells.Contains(spell.Name))
            {
                Lua.LuaDoString("CastSpellByName(\"" + spell.NameInGame + "\")");
                ClickOnTerrain.Pulse(unit.Position);
            }
            else
            {
                //	Face unit to cast
                if (unit.Guid != ObjectManager.Me.Guid)
                    MovementManager.Face(unit.Position);

                if (unit.Type == WoWObjectType.Player)
                    SpellManager.CastSpellByNameOn(spell.NameInGame, unit.Name);
                if (unit.Guid == ObjectManager.Target.Guid)
                    SpellManager.CastSpellByNameOn(spell.NameInGame, "target");
                else
                {
                    var old = ObjectManager.Me.MouseOverGuid;
                    ObjectManager.Me.MouseOverGuid = unit.Guid;
                    SpellManager.CastSpellByNameOn(spell.NameInGame, "mouseover");
                    ObjectManager.Me.MouseOverGuid = old;
                }
            }
            return true;
        }

        /*
         *public static float Cooldown(this wManager.Wow.Class.Spell spell)
            {
                return spell.Cooldown();
            }
        */

        /*
       public static bool CastSpell(this Spell spell, WoWUnit unit,bool face = true, bool force = false)
       {
           //bool alreadyCasting = ObjectManager.Me.CastingSpell;
           //  Handle bullshit units
           //  Note To Self: Do I want to handle "unit.IsDead" differently? Should I let that case pass to "force" parameter as 
           if (unit == null || !unit.IsValid || unit.IsDead || !spell.KnownSpell || !spell.IsSpellUsable || !Check.LoS(unit))
           {
               return false;
           }

           //  already casting and force disabled - 
           if (ObjectManager.Me.IsCast && !force)
           {
               Logging.Write($"[{spell.Name}] Already casting ({ObjectManager.Me.CastingSpell.NameInGame}) and 'force' is disabled");
               return false;
           }

           //  Stop moving if spell needs to be cast
           if (spell.CastTime > 0)
           {
               MovementManager.StopMoveTo(false, Usefuls.LatencyReal + 10);
           }

           //  Stop casting if force is enabled and unit is casting
           if (force && ObjectManager.Me.IsCast)
           {

               wManager.Wow.Helpers.Move.Forward(timeMs:1);
               //Lua.LuaDoString("SpellStopCasting();");
               //robotManager.Helpful.Keyboard.PressKey(wManager.Wow.Enums.Keybindings.MOVEFORWARD);
           }

           //  face the target if we need to
           if (!ObjectManager.Me.IsFacing(unit.Position) && face)            {                MovementManager.Face(unit.Position);            }

           //  cast AoE spell on position
           if (AoESpells.Contains(spell.Name))
           {
               Lua.LuaDoString("CastSpellByName(\"" + spell.NameInGame + "\")");
               ClickOnTerrain.Pulse(unit.Position);
               return true;
           }

           // cast spell on self
           if (unit.IsLocalPlayer)
           {
               SpellManager.CastSpellByNameOn(spell.NameInGame, unit.Name);
               return true;
           }

           if (unit.IsMyTarget)
           {
               SpellManager.CastSpellByNameOn(spell.NameInGame, "target");
               return true;
           }            if (unit.Guid128.WoWType == wManager.Wow.Enums.GuidType.Player)
           {
               SpellManager.CastSpellByNameOn(spell.NameInGame, unit.Name);
               return true;
           }            if (unit.GetDistance <= spell.MaxRange && Check.LoS(unit))
           {
               var temp = ObjectManager.Me.FocusGuid;
               ObjectManager.Me.FocusGuid = unit.Guid;
               SpellManager.CastSpellByNameOn(spell.NameInGame, "focus");
               ObjectManager.Me.MouseOverGuid = temp;
               Logging.Write($"Mouseover cast spell '{spell.NameInGame}' on '{unit.Name}' (guid:{unit.Guid.ToString()})");
           }
           return false;
       }
       */

    }
}