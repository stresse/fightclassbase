﻿using robotManager;
using robotManager.Helpful;
using robotManager.FiniteStateMachine;
using wManager;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using MemoryRobot;


namespace Uhtred
{
    public class Locate
    {
        public static Vector3 ClosestPositionWithLoS(WoWObject o)
        {
            var path = PathFinder.FindPath(o.Position);
            for (int i = 1; i < path.Count; i++)
            {
                var point = path[i];
                if (Check.LoS(point,o.Position))
                {
                    return point;
                }
            }
            return null;
        }

        public static IEnumerable<WoWObject> FindObjects(string objectName)
        {
            return ObjectManager.GetWoWGameObjectByName(objectName);
        }

        public static WoWObject FindObjectInRange(string objectName)
        {
            return FindObjects(objectName).OrderBy(o => Check.LoS(o) && Check.Range(o)).FirstOrDefault();
        }

        public static WoWObject FindObject(ulong guid)
        {
            
            return new WoWObject(guid);
        }

        public static Vector3 GetPosition(WoWObject woWObject)
        {
            return woWObject.Position;
        }
        public static Vector3 GetPosition(string s)
        {
            WoWObject bestTarget = FindObjectInRange(s);
            return bestTarget.Position;
        }

        public static Vector3 GetPosition(Vector3 postion)
        {
            Log($"You're casting to 'Uhtred.Specials.GetPosition to get the position of a Vector (which is either sub-optimal or retarded)");
            return postion;
        }

        public static float GetDistance(Vector3 position1, Vector3 position2)
        {
            return Vector3.Distance(position1, position2);
        }

        public static float GetDistance(WoWObject o1, WoWObject o2)
        {
            Vector3 position1 = GetPosition(o1);
            Vector3 position2 = GetPosition(o2);
            return Vector3.Distance(position1, position2);
        }

        public static float GetDistance(Vector3 position)
        {
            return GetDistance(ObjectManager.Me.Position, position);

            // this could also be written as 
            // ObjectManager.Me.Position(position)
        }

        public static Logger Logger = new Logger("Locate");
        public static void Log(params object[] inputs)
        {
            Logger.Log(inputs);
        }

    }
}