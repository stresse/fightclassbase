using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;
using wManager.Wow.Enums;

namespace Uhtred.Specials
{
    public static class Tools
    {
        public static bool CheckLoS(Vector3 point1, Vector3 point2)
        {
            return !TraceLine.TraceLineGo(point1, point2, CGWorldFrameHitFlags.HitTestLOS);
        }
        public static bool CheckRange(Vector3 point1, Vector3 point2, float maxDist = 45, float minDist = 0)
        {
            float d = Locate.GetDistance(point1, point2);
            if (d > minDist && d < maxDist)
                return true;
            return false;
        }
    }
}

namespace Uhtred.Specials.ByClass
{
    public static class DemonHunter
    {

    }
}
namespace Uhtred.Movement
{
    public static class Special
    {
        // I use this to repetetively push buttons for addon-interaction and whatnot
        // PLAN: impllement it as a "stop casting" feature to remove injection
        public static void PressKey(string key)
        {
            Keyboard.PressKey(wManager.Wow.Memory.WowMemory.Memory.WindowHandle, key);
        }

        public static void DemonHunterGlide()
        {
            wManager.Events.MovementEvents.OnMovementPulse += (path, c) =>
            {
                // saving our current position
                Vector3 myPosition = ObjectManager.Me.Position;

                
                var furthestPointInLoS = path.Where(p => !TraceLine.TraceLineGo(myPosition, p, wManager.Wow.Enums.CGWorldFrameHitFlags.HitTestAll)).OrderBy(p => ObjectManager.Me.Position.DistanceTo(p)).LastOrDefault();
                var distance = ObjectManager.Me.Position.DistanceTo(furthestPointInLoS);
                Spell FelRush = new wManager.Wow.Class.Spell("Fel Rush");
                Logging.WriteDebug($@"Beginning Demon Hunter 'Glide' ability with a jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                Thread.Sleep(500);
                Logging.Write(@"Jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                Logging.Write("NEED SOMETHING HERE");
                Thread.Sleep(500);
                Logging.Write(@"Jump");
                wManager.Wow.Helpers.Move.JumpOrAscend();
                FelRush.Launch();
                Logging.Write(@"Done");
                Logging.Write("MaxDistance= " + ObjectManager.Me.Position.DistanceTo(furthestPointInLoS) + ", Point = " + furthestPointInLoS.ToString());
            };
        }
    }

}
namespace Uhtred.Items
{
    public static class Special
    {
        public static void PutSafariHatOn()
        {
            ItemsManager.UseItem("Safari Hat");
        }
        public static int Count(string name)
        {
            return ItemsManager.GetItemCountByNameLUA(name);
        }
        public static bool HaveItem(string name)
        {
            return ItemsManager.GetItemCountByNameLUA(name) >= 1;
        }
        public static void Delete(string itemName)
        {
            // this is just lua code, same as you would use in a WoW macro
            string luaCode = $@"  for bag=0,4 do
                                        for slot=1, GetContainerNumSlots(b) do
                                            local itemLink = GetContainerItemLink(b, s)
                                            if itemLink and string.find(itemLink, '{itemName}') then
                                                PickupContainerItem(b, s);
                                                DeleteCursorItem();
                                            end
                                        end
                                    end";
            // this is one of two ways to run lua code; the other ('Lua.LuaDoString') is 
            // is best for when you want it to return variables, this is faster for execution
            Lua.RunMacroTextHW(luaCode);
        }
        public static bool Use(this string name)
        {
            if (!HaveItem(name))
                return false;
            ItemsManager.UseItem(name);
            return true;
        }

    }
}

 
/*
namespace WowUI
{
    public class Frames
    {
        string createFrameString = @"local frame = CreateFrame(""{0}"", ""{1}"", {2}, ""{3}"", {4})" ; //	frameRef, frameType, frameName, parentFrame, inheritsFrame, id
        static void CreateFrame(string frameRef, string frameType, string frameName, string parentFrame, string inheritsFrame, int id)
        {
            Lua.LuaDoString(createFrameString.Format(frameRef, frameType, frameName, parentFrame, inheritsFrame, id));
        }
    }
}
*/