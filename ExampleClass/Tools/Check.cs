﻿using robotManager;
using robotManager.Helpful;
using robotManager.FiniteStateMachine;
using wManager;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using MemoryRobot;

namespace Uhtred
{
    public static class Check
    {
        public static bool LoS(Vector3 v1, Vector3 v2)
        {
            return !TraceLine.TraceLineGo(v1, v2, CGWorldFrameHitFlags.HitTestLOS);
        }

        public static bool LoS(Vector3 position)
        {
            return !TraceLine.TraceLineGo(ObjectManager.Me.Position, position, CGWorldFrameHitFlags.HitTestLOS);
        }

        public static bool LoS(WoWObject o1, WoWObject o2)
        {
            return LoS(o1.Position, o2.Position);

        }

        public static bool LoS(WoWObject target)
        {
            return LoS(ObjectManager.Me.Position, target.Position);
        }

        public static bool Range(Vector3 position1, Vector3 position2, float Range = 45)
        {
            return Find.Distance(position1, position2) <= Range;
        }

        public static bool Range(Vector3 position1, Vector3 position2, Spell spell)
        {
            return Find.Distance(position1, position2) <= spell.MaxRange;
        }

        public static bool Range(Vector3 position, Spell spell)
        {
            return Find.Distance(ObjectManager.Me.Position, position) <= spell.MaxRange;
        }
        public static bool Range(WoWObject target, Spell spell)
        {
            return Find.Distance(ObjectManager.Me.Position, target.Position) <= spell.MaxRange;
        }
        public static bool Range(WoWObject o1, WoWObject o2)
        {
            return Range(o1.Position, o2.Position);
        }

        public static bool Range(Vector3 vector)
        {
            return Range(ObjectManager.Me.Position, vector);
        }

        public static bool Range(WoWObject target)
        {
            return Range(target.Position);
        }

        public static uint ManaRequirement(string spellName)
        {
            string manaRequirement = $@"
                local info = GetSpellPowerCost({spellName})
                for i=1,#info do
                    if info[i]['name'] == 'MANA' then
                        return info[i]['cost']
                    end
                end
                return 0";
            return Lua.LuaDoString<uint>(manaRequirement);

        }

        public static Logger Logger = new Logger("Check");
        public static void Log(params object[] inputs)
        {
            Logger.Log(inputs);
        }

    }
}