﻿using robotManager;
using robotManager.Helpful;
using robotManager.FiniteStateMachine;
using wManager;
using wManager.Wow.Class;
using wManager.Wow.Enums;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using MemoryRobot;
using Uhtred;



namespace Uhtred
{
    public class Find
    {
        int i = 0;

        public static IEnumerable<WoWObject> Objects(string objectName)
        {
            return ObjectManager.GetWoWGameObjectByName(objectName);
        }

        public static WoWObject Object(string objectName)
        {
            return Objects(objectName).OrderBy(o => Check.Range(o) && Check.LoS(o)).FirstOrDefault();
        }

        public static WoWObject Object(ulong guid)
        {
            
            return new WoWObject(guid);
        }

        public static Vector3 Position(WoWObject woWObject)
        {
            return woWObject.Position;
        }
        public static Vector3 Position(string s)
        {
            WoWObject bestTarget = Object(s);
            return bestTarget.Position;
        }

        public static Vector3 Position(Vector3 postion)
        {
            var Log = new Logger("Uhtred");
            Log.Log($"You're casting to 'Uhtred.Specials.GetPosition to get the position of a Vector (which is either sub-optimal or retarded)");
            return postion;
        }

        public static float Distance(Vector3 position1, Vector3 position2)
        {
            return Vector3.Distance(position1, position2);
        }

        public static float Distance(WoWObject o1, WoWObject o2)
        {
            return Distance(o1.Position, o2.Position);
        }

        public static float Distance(Vector3 position)
        {
            return Distance(ObjectManager.Me.Position, position);

            // this could also be written as 
            // ObjectManager.Me.Position(position)
        }
    }

}