﻿using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;
using System.Text;

// PLUGIN FUNCTIONALITY
namespace Uhtred
{
    static class FormatTo
    {
        public static string Lua(string input)
        {
            return input.Replace("'", "\\'").Replace("\"", "\\\"");
        }

        public static string String(params object[] inputs)
        {
            int errorCount = 0;
            string logString = "";
            foreach (object input in inputs)
            {
                try
                {
                    logString += input.ToString();
                }
                catch (Exception e)
                {
                    logString += $"[-ERROR-{e} -ERROR-]";
                    errorCount ++;
                }
            }
            if (errorCount > 0)
            {
                logString += $"\n(Error Count : {errorCount})";
            }
            return logString;
            
        }
    }

    public class LuaTools
    {
        private static string LuaAndCondition(string[] names, string varname)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var name in names)
            {
                sb.Append(" and " + varname + " == \"" + name + "\"");
            }

            return sb.ToString().Substring(5);
        }

        private static string LuaOrCondition(string[] names, string varname)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var name in names)
            {
                sb.Append(" or " + varname + " == \"" + name + "\"");
            }

            return sb.ToString().Substring(4);
        }

        private static string LuaTable(string[] names)
        {
            string returnValue = "{";
            foreach (var name in names)
            {
                returnValue += "[\"" + name + "\"] = false,";
            }

            return returnValue += "};";
        }
    }

    public class Logger
    {
        string _loggerName;
        string _loggerHeader;
        string _loggerHeaderLua;
        string _logString;
        string _logStringLua;
        public Logger(string loggerName)
        {
            _loggerName = loggerName;
            _loggerHeader = $"[{loggerName}]";
            _loggerHeaderLua = $"|cFFFF0000{_loggerHeader}|r";
        }
        public void Log(string message)
        {
            _logString = message;
            _logStringLua = FormatTo.Lua($"{_loggerHeaderLua} {message}");
            //string logStringLua = FormatTo.Lua($"{_loggerHeaderLua} {message}");
            Logging.Write(_logStringLua);
            Logging.Write($"{_loggerHeader} {message}");
            //Logging.Write(logStringLua);
            Lua.LuaDoString($"DEFAULT_CHAT_FRAME:AddMessage('{_logStringLua}')");
        }
        public void Log(params object[] inputs)
        {
            Log(FormatTo.String(inputs));
        }
    }
}