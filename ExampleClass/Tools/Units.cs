﻿using robotManager;
using wManager.Wow.Class;
using robotManager.Helpful;
using wManager.Wow.Helpers;
using wManager.Wow.ObjectManager;
using wManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using robotManager.FiniteStateMachine;
using MemoryRobot;

namespace Uhtred
{
    public class Enemies
    {
        public static Int128 CurrentTarget
        {
            get => ObjectManager.Me.Target;
            set => ObjectManager.Me.Target = value;
        }

        public static IEnumerable<WoWUnit> RelevantUnits => ObjectManager.GetObjectWoWUnit().Where(u => u.IsRelevant());
        public static IEnumerable<WoWUnit> RelevantUnitsInRange(int range) => RelevantUnits.Where(u => u.IsInRange(range));
        public static IEnumerable<WoWUnit> RelevantUnitsInRangeAndLoS(int range) => RelevantUnitsInRange(range).Where(u => u.IsInLoS());




    }

    public static class UnitChecks
    {
        static float defaultRange = 40f;
        //        static bool IsRelevant(WoWUnit u) => u != null && u.IsValid && u.IsAttackable && !u.IsBlacklisted && u.IsAlive;
        //static Predicate<WoWUnit> IsRelevant = u => u != null && u.IsValid && u.IsAlive && u.IsAttackable && !u.IsBlacklisted;
        //static Predicate<WoWUnit> IsInRange = u => IsRelevant(u) && ;

        public static bool IsRelevant(this WoWUnit u) => u != null && u.IsValid && u.IsAlive && u.IsAttackable && !u.IsBlacklisted;
        //public static bool IsInRange(WoWUnit u, float r) => u.GetDistance < r;
        public static bool IsInRange(this WoWUnit u, float r) => u.GetDistance < r;
        public static bool IsInLoS(this WoWUnit u) => Check.LoS(u);
        public static bool IsInRangeAndLoS(this WoWUnit u, float r) => IsRelevant(u) && IsInRange(u, r) && IsInLoS(u);
    }
}